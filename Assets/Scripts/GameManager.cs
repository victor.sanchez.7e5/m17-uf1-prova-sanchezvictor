using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject alienFragate;
    private GameObject alienSpeedfligth;
    private GameObject shipMisile;
    private GameObject fireballPlayer;
    private GameObject fireballalienSpeedflight;
    private GameObject fireballSuelta; 


    // Start is called before the first frame update
    void Start()
    {
        alienFragate = GameObject.Find("Alien - Fragate");
        alienSpeedfligth = GameObject.Find("Alien - SpeedFlight");
        shipMisile = GameObject.Find("2DShipsMissileMedium"); 

        fireballPlayer = GameObject.Find("fireballPlayer");
        fireballalienSpeedflight = GameObject.Find("fireball-alienSpeedFlight");
        fireballSuelta = GameObject.Find("fireballSuelta");

        alienFragate.SetActive(false);
        alienSpeedfligth.SetActive(true);
        shipMisile.SetActive(false);

        fireballPlayer.SetActive(true);
        fireballalienSpeedflight.SetActive(false);
        fireballSuelta.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
