using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienSpeedFlight : MonoBehaviour
{


    public float nextspawntime;
    [SerializeField]
    public GameObject alien;
    [SerializeField]
    private float spawnDelay = 10f;

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(true); 
    }

    // Update is called once per frame
    void Update()
    {

        if (ShouldSpawn())
        {
            Spawn();
            spawnDelay = 9999f;
            nextspawntime = 9999f;
        }

    }

    /*public void GenerateEnemies()
    {
        Rigidbody2D a = Instantiate(alien, new Vector3(Random.Range(-10, 10), Random.Range(7, 3), 0), transform.rotation);
        a.velocity = transform.position * 2;

        
    }*/

    /*public void DestroyAliens()
    {

    }
    */


    private void Spawn()
    {
        nextspawntime = Time.time + spawnDelay;
        Instantiate(alien, new Vector3(Random.Range(-10, 10), Random.Range(7, 3), 0), transform.rotation);
    }

    private bool ShouldSpawn()
    {
        return Time.time > nextspawntime;
    }

}
    
