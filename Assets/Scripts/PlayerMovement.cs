using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float velocitat = 7.5f;

    public Rigidbody2D nauEspacialRigidbody;

    //public GameObject fireballBullet;

    public Rigidbody2D projectile;
    public float speed = 4;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Set that position to the Figther Canon
        float playerPositionX = Input.GetAxis("Horizontal") * velocitat;
        float playerPositionY = Input.GetAxis("Vertical") * velocitat;


        nauEspacialRigidbody.velocity = new Vector2(playerPositionX, playerPositionY);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Call shoot method
            ShootingBullet();

        }

        CameraMarginsSet();

    }

    public void ShootingBullet()
    {

        //Instantiate(projectile, new Vector2(canonXposition, canonYposition), Quaternion.identity);

        Rigidbody2D p = Instantiate(projectile, transform.position, transform.rotation);
        p.velocity = transform.position*5;
            
    }

    public void CameraMarginsSet()
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);

    }


}
